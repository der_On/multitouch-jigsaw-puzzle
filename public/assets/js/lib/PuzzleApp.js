import Storage from './Storage.js';
import utils from './utils.js';

const FLICKR_API_KEY = '7ecdb5b22072ec1493dcbdba230fb557';
const imageSearchUrl = 'https://api.flickr.com/services/rest?api_key={FLICKR_API_KEY}&method=flickr.photos.search&text={q}&per_page=500&format=json&nojsoncallback=1&privacy_filter=1&safe_search=1';
const imageSearchPreviewUrl = 'https://farm{farm}.staticflickr.com/{server}/{id}_{secret}_n.jpg';
const imageSearchOriginalUrl = 'https://farm{farm}.staticflickr.com/{server}/{id}_{secret}_b.jpg';

const PuzzleApp = function (options = {}) {
  const app = {
    imageURI: null,
    tilesCount: null,
    cols: null,
    rows: null,
    tiles: [],
    image: null,
    imageRatio: null,
    imageWidth: null,
    imageHeight: null,
    ghost: false,
    headerCollapsed: false,
    touches: {}
  };
  const form = options.form;
  const puzzleContainer = options.puzzleContainer;

  app.init = function () {
    if (!app.getShortCode()) {
      app.generateShortCode();
    }

    app.storage = Storage(app.getShortCode());

    form.ghost.addEventListener('click', app.handleGhostClick);
    form.addEventListener('submit', app.handelFormSubmit);
    form.image_uri.addEventListener('input', utils.debounce(app.handleImageUriInput, 1000));
    window.addEventListener('resize', app.renderPuzzle);

    app.generateFromStorage();
  };

  app.toggleHeader = function () {
    app.headerCollapsed = !app.headerCollapsed;
    app.storage.setItem('headerCollapsed', app.headerCollapsed);
    app.renderHeader();
  };

  app.renderHeader = function () {
    const header = document.getElementById('header');
    if (app.headerCollapsed) {
      header.classList.add('is-collapsed');
    } else {
      header.classList.remove('is-collapsed');
    }
    const headerToggle = document.getElementById('header-toggle');

    headerToggle.innerHTML = app.headerCollapsed ? 'show' : 'hide';
  };

  app.generateShortCode = function () {
    window.location.hash = '#' + utils.shortCode();
  };

  app.getShortCode = function () {
    return _.trimStart(window.location.hash, '#');
  };

  app.generateFromStorage = function () {
    app.imageURI = app.storage.getItem('imageURI', null);
    app.tilesCount = app.storage.getItem('tilesCount', null);
    app.tiles = app.storage.getItem('tiles', []);
    app.ghost = app.storage.getItem('ghost', false);
    app.headerCollapsed = app.storage.getItem('headerCollapsed');
    app.renderHeader();

    form.ghost.checked = app.ghost;

    if (app.imageURI && app.tilesCount) {
      form.image_uri.value = app.imageURI;
      form.tiles_count.value = app.tilesCount;
      form.ghost.checked = app.ghost;

      app.loadImage()
      .then(() => {
        app.generateTileElements();
        app.renderPuzzle();
      });
    }
  };

  app.handleImageUriInput = function (event) {
    const q = event.target.value.trim();
    if (
      q.length < 3 ||
      q.indexOf('https://') === 0 ||
      q.indexOf('http://') === 0 ||
      q.indexOf('file://') === 0
    ) {
      return;
    }

    app.clearImageSearchPreview();
    const imageSearchPreviewEl = document.getElementById('image-search-preview');
    imageSearchPreviewEl.classList.add('is-loading');

    const url = imageSearchUrl
    .replace('{FLICKR_API_KEY}', FLICKR_API_KEY)
    .replace('{q}', q);

    fetch(url)
    .then((res) => {
      imageSearchPreviewEl.classList.remove('is-loading');
      if (res.ok) {
        return res.json();
      } else {
        throw new Error('Error during image search.');
      }
    })
    .then((result) => {
      const photos = result.photos ? result.photos.photo || [] : [];
      app.renderImageSearchPreview(photos);
    })
    .catch((err) => {
      imageSearchPreviewEl.classList.remove('is-loading');
      console.error(err);
    });
  };

  app.renderImageSearchPreview = function (photos) {
    const imageSearchPreviewEl = document.getElementById('image-search-preview');

    const html = photos.reduce((html, photo) => {
      const imagePreviewUrl = imageSearchPreviewUrl
      .replace('{farm}', photo.farm)
      .replace('{server}', photo.server)
      .replace('{secret}', photo.secret)
      .replace('{id}', photo.id);
      const imageOriginalUrl = imageSearchOriginalUrl
      .replace('{farm}', photo.farm)
      .replace('{server}', photo.server)
      .replace('{secret}', photo.secret)
      .replace('{id}', photo.id);
      return html + `<a class="image-search-preview__image" href="javascript:puzzleApp.setImageUri('${imageOriginalUrl}');puzzleApp.clearImageSearchPreview();">
        <img src="${imagePreviewUrl}" alt="${photo.title}" loading="lazy">
      </a>`;
    }, '');

    imageSearchPreviewEl.innerHTML = html;
  };

  app.setImageUri = function (imageUri) {
    form.image_uri.value = imageUri;
  };

  app.clearImageSearchPreview = function () {
    const imageSearchPreviewEl = document.getElementById('image-search-preview');
    imageSearchPreviewEl.innerHTML = '';
  };

  app.handelFormSubmit = function (event) {
    event.stopPropagation();

    app.imageURI = form.image_uri.value.trim();
    app.tilesCount = parseInt(form.tiles_count.value.trim(), 10);
    app.tiles = [];
    app.storage.setItem('imageURI', app.imageURI);
    app.storage.setItem('tilesCount', app.tilesCount);

    app.generatePuzzle();

    return false;
  };

  app.handleGhostClick = function (event) {
    app.ghost = form.ghost.checked;
    app.storage.setItem('ghost', app.ghost);
    app.renderGhost();
  };

  app.generatePuzzle = function () {
    app.loadImage()
    .then(() => {
      app.generateTiles();
      app.generateTileElements();
      app.renderPuzzle();
    });
  };

  app.loadImage = function () {
    return new Promise((resolve, reject) => {
      app.image = new Image();
      app.image.addEventListener('load', () => {
        app.imageWidth = app.image.naturalWidth;
        app.imageHeight = app.image.naturalHeight;
        app.imageRatio = app.imageWidth / app.imageHeight;

        // landscape
        if (app.imageRatio > 1) {
          app.cols = app.tilesCount;
          app.rows = app.tilesCount / app.imageRatio;
        }
        // portrait
        else {
          app.rows = app.tilesCount;
          app.cols = app.tilesCount * app.imageRatio;
        }
        resolve();
      });

      app.image.src = app.imageURI;
    });
  };

  app.iterateTiles = function (cb) {
    const rows = Math.round(app.rows);
    const cols = Math.round(app.cols);

    for(let row = 0; row < rows; row++) {
      for(let col = 0; col < cols; col++) {
        cb(col, row);
      }
    }
  };

  app.generateTiles = function () {
    const columnCount = Math.round(app.cols);
    const rowCount = Math.round(app.rows);

    const rows = utils.buildDistributions(rowCount, columnCount);

    const columns = utils.buildDistributions(columnCount, rowCount);
    utils.offsetPoints(columns, function(point, j, i) {
      return utils.transposePoint(point);
    });

    const offsetNormalize = _.partial(utils.offsetPoint, -0.5, -0.5);
    const offsetRight = _.partial(utils.offsetPoint, 1, 0.0);
    const offsetDown = _.partial(utils.offsetPoint, 0.0, 1.0);

    app.iterateTiles((col, row) => {
      if (!app.tiles[row]) {
        app.tiles.push([]);
      }

      const edges = [];
      edges.push(rows[row][col].map(offsetNormalize));
      edges.push(columns[col + 1][row].map(offsetNormalize).map(offsetRight));
      edges.push(rows[row + 1][col].slice().reverse().map(offsetNormalize).map(offsetDown));
      edges.push(columns[col][row].slice().reverse().map(offsetNormalize));

      let x;
      const y = _.random(0, 1 - (1 / app.rows), true);
      const margin = 0.25;

      // left
      if (_.random(0, 1, false)) {
        x = _.random(-margin, -(1 / app.cols));
      }
      // right
      else {
        x = _.random(1, 1 + margin - (1 / app.cols));
      }

      const tile = {
        x: x,
        y: y,
        z: 0,
        angle: 0,
        connections: {
          top: false,
          left: false,
          bottom: false,
          right: false
        },
        edges: edges
      };

      app.tiles[row].push(tile);
    });

    app.storage.setItem('tiles', app.tiles);
  };

  app.generateTileElements = function () {
    // clear eventually solved state
    puzzleContainer.classList.remove('is-solved');

    // clear existing tiles
    Array.from(puzzleContainer.querySelectorAll('.puzzle-tile'))
    .forEach((el) => {
      el.remove();
    });

    app.iterateTiles((col, row) => {
      const tile = app.getTile(col, row);
      // console.log(tile.edges.map((edge => edge.join(' '))));
      // console.log(utils.clipPath(tile.edges));
      const tileSvg = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        version="1.1"
        viewBox="-1 -1 2 2"
        >
        <path stroke="#000" stroke-width="0.01" fill="#000" d="${utils.clipPath(tile.edges)}"></path>
      </svg>`;
      const tileSvgEncoded = `data:image/svg+xml;base64,${btoa(tileSvg)}`;
      const tileEl = document.createElement('div');
      // const maskFileName = _.padStart(row % 7, 2, '0') + _.padStart(col % 7, 2, '0');

      tileEl.setAttribute('id', `puzzle-tile-${col}-${row}`);
      tileEl.setAttribute('data-col', col);
      tileEl.setAttribute('data-row', row);
      tileEl.classList.add('puzzle-tile');

      const shadowEl = document.createElement('div');
      shadowEl.classList.add('puzzle-tile__shadow');
      shadowEl.style.maskImage = `url('${tileSvgEncoded}')`;
      shadowEl.style.webkitMaskImage = `url('${tileSvgEncoded}')`;
      tileEl.appendChild(shadowEl);

      const tileImageWrapper = document.createElement('div');
      tileImageWrapper.classList.add('puzzle-tile__image');
      tileImageWrapper.style.maskImage = `url('${tileSvgEncoded}')`;
      tileImageWrapper.style.webkitMaskImage = `url('${tileSvgEncoded}')`;

      tileEl.appendChild(tileImageWrapper);

      const tileImage = new Image();
      tileImage.src = app.imageURI;
      tileImageWrapper.appendChild(tileImage);

      tileEl.addEventListener('touchstart', app.handleTouchStart);
      tileEl.addEventListener('mousedown', app.handleMouseDown);
      tileEl.addEventListener('touchend', app.handleTouchEnd);
      tileEl.addEventListener('touchcancel', app.handleTouchCancel);
      tileEl.addEventListener('touchmove', app.handleTouchMove);

      puzzleContainer.appendChild(tileEl);
    });
  };

  app.getTileSize = function () {
    const puzzleWidth = puzzleContainer.offsetWidth;
    const puzzleHeight = puzzleContainer.offsetHeight;
    const puzzleRatio = puzzleWidth / puzzleHeight;
    let tileSize;

    // image is wider then puzzle
    if (puzzleRatio >= 1) {
      tileSize = puzzleWidth / Math.round(app.cols);
    } else {
      tileSize = puzzleHeight / Math.round(app.rows);
    }

    return tileSize;
  };

  app.renderPuzzle = function () {
    const puzzleAreaWidth = puzzleContainer.parentElement.offsetWidth;
    const puzzleAreaHeight = puzzleContainer.parentElement.offsetHeight;
    const scaleX = puzzleAreaWidth / app.imageWidth;
    const scaleY = puzzleAreaHeight / app.imageHeight;
    const scale = Math.min(scaleX, scaleY);

    puzzleContainer.style.width = (app.imageWidth * scale) + 'px';
    puzzleContainer.style.height = (app.imageHeight * scale) + 'px';
    puzzleContainer.querySelector('.puzzle-ghost').style.backgroundImage = `url(${app.imageURI})`;

    app.renderTiles();
    app.renderGhost();
  };

  // updates tile size and position relative to screen dimensions
  app.renderTiles = function () {
    const tileSize = app.getTileSize();

    app.iterateTiles(
      _.partialRight(app.renderTile, tileSize)
    );
  };

  app.renderTile = function (col, row, tileSize = null) {
    const tileSizeInner = tileSize || app.getTileSize();
    const tileSizeOuter = tileSizeInner * 2;
    const tile = app.getTile(col, row);
    const tileLeft = tile.x;
    const tileTop = tile.y;
    const tileEl = document.getElementById(`puzzle-tile-${col}-${row}`);
    tileEl.style.width = tileSizeInner + 'px';
    tileEl.style.height = tileSizeInner + 'px';
    tileEl.style.left = (tileLeft * 100) + '%';
    tileEl.style.top = (tileTop * 100) + '%';
    tileEl.style.zIndex = (app.getTile(col, row).z || 0) + 1;

    ['top', 'left', 'bottom', 'right'].forEach((dir) => {
      if (tile.connections[dir]) {
        tileEl.classList.add(`has-connection-${dir}`);
      } else {
        tileEl.classList.remove(`has-connection-${dir}`);
      }
    });

    const shadowEl = tileEl.querySelector('.puzzle-tile__shadow');
    shadowEl.style.maskSize = `${tileSizeOuter}px ${tileSizeOuter}px`;
    shadowEl.style.webkitMaskSize = `${tileSizeOuter}px ${tileSizeOuter}px`;

    const tileImageWrapper = tileEl.querySelector('.puzzle-tile__image');
    tileImageWrapper.style.maskSize = `${tileSizeOuter}px ${tileSizeOuter}px`;
    tileImageWrapper.style.webkitMaskSize = `${tileSizeOuter}px ${tileSizeOuter}px`;

    const tileImage = tileImageWrapper.querySelector('img');
    tileImage.style.width = (100 * app.cols / 2) + '%';
    tileImage.style.height = (100 * app.rows / 2) + '%';
    tileImage.style.left = (-(col * tileSizeInner) + (tileSizeInner / 2)) + 'px';
    tileImage.style.top = (-(row * tileSizeInner) + (tileSizeInner / 2)) + 'px';
  };

  app.renderGhost = function () {
    const ghostEl = puzzleContainer.querySelector('.puzzle-ghost');
    if (app.ghost) {
      ghostEl.classList.add('is-visible');
    } else {
      ghostEl.classList.remove('is-visible');
    }
  };

  app.connectTiles = function (
    colA, rowA,
    colB, rowB
  ) {
    const tileAData = app.getTile(colA, rowA);
    const tileBData = app.getTile(colB, rowB);
    const tileSizeFractionX = 1 / app.cols;
    const tileSizeFractionY = 1 / app.rows;
    let hasConnected = false;

    // make A stick to B
    // left
    if (colB === colA - 1) {
      tileAData.connections.left = true;
      tileBData.connections.right = true;
      tileAData.x = tileBData.x + tileSizeFractionX;
      tileAData.y = tileBData.y;
      hasConnected = true;
    }
    // right
    else if (colB === colA + 1) {
      tileAData.connections.right = true;
      tileBData.connections.left = true;
      tileAData.x = tileBData.x - tileSizeFractionX;
      tileAData.y = tileBData.y;
      hasConnected = true;
    }
    // top
    else if (rowB === rowA - 1) {
      tileAData.connections.top = true;
      tileBData.connections.bottom = true;
      tileAData.x = tileBData.x;
      tileAData.y = tileBData.y + tileSizeFractionY;
      hasConnected = true;
    }
    // bottom
    else if (rowB === rowA + 1) {
      tileAData.connections.bottom = true;
      tileBData.connections.top = true;
      tileAData.x = tileBData.x;
      tileAData.y = tileBData.y - tileSizeFractionY;
      hasConnected = true;
    }

    if (!hasConnected) {
      return;
    }

    app.alignConnectedTiles(colB, rowB);

    app.renderTiles();

    // make tiles glow
    const tileAEl = document.getElementById(`puzzle-tile-${colA}-${rowA}`);

    tileAEl.classList.add('is-connecting');

    setTimeout(() => {
      tileAEl.classList.remove('is-connecting');
    }, 250);

    // make changes persistant
    app.storage.setItem('tiles', app.tiles);
  };

  app.autoConnectTile = function (col, row) {
    const cols = Math.round(app.cols);
    const rows = Math.round(app.rows);
    const tile = app.getTile(col, row);
    const tileSizeFractionX = 1 / app.cols;
    const tileSizeFractionY = 1 / app.rows;

    // ignore tiles that are not within the puzzle area
    if (
      tile.x < -tileSizeFractionX || tile.x > 1 ||
      tile.y < -tileSizeFractionY || tile.y > 1
    ) {
      return;
    }

    // try to connect left
    if (
      !tile.connections.left && col > 0 &&
      app.areTilesClose(col, row, col - 1, row, 'left')
    ) {
      app.connectTiles(col, row, col - 1, row);
    }
    // try to connect right
    else if (
      !tile.connections.right && col < cols - 1 &&
      app.areTilesClose(col, row, col + 1, row, 'right')
    ) {
      app.connectTiles(col, row, col + 1, row);
    }
    // try to connect top
    else if (
      !tile.connections.top && row > 0 &&
      app.areTilesClose(col, row, col, row - 1, 'top')
    ) {
      app.connectTiles(col, row, col, row - 1);
    }
    // try to connect bottom
    else if (
      !tile.connections.bottom && row < rows - 1 &&
      app.areTilesClose(col, row, col, row + 1, 'bottom')
    ) {
      app.connectTiles(col, row, col, row + 1);
    }
  };

  app.areTilesClose = function(
    colA, rowA,
    colB, rowB,
    edge
  ) {
    const margin = 5; // 5px margin
    const tileSize = app.getTileSize();
    const tileA = document.getElementById(`puzzle-tile-${colA}-${rowA}`);
    const tileB = document.getElementById(`puzzle-tile-${colB}-${rowB}`);
    const boundsA = tileA.getBoundingClientRect();
    const boundsB = tileB.getBoundingClientRect();
    const aLeft = boundsA.left - margin + (edge === 'right' ? tileSize * 0.9 : 0);
    const aRight = boundsA.right + margin - (edge === 'left' ? tileSize * 0.9 : 0);

    const aTop = boundsA.top - margin + (edge === 'bottom' ? tileSize * 0.9 : 0);
    const aBottom = boundsA.bottom + margin - (edge === 'top' ? tileSize * 0.9 : 0);

    const bLeft = boundsB.left - margin + (edge === 'left' ? tileSize * 0.9 : 0);
    const bRight = boundsB.right + margin - (edge === 'right' ? tileSize * 0.9 : 0);

    const bTop = boundsB.top - margin + (edge === 'top' ? tileSize * 0.9 : 0);
    const bBottom = boundsB.bottom + margin - (edge === 'bottom' ? tileSize * 0.9 : 0);

    return !(
      bLeft > aRight ||
      bRight < aLeft ||
      bTop > aBottom ||
      bBottom < aTop
    );
  };

  app.tileIsConnected = function (col, row) {
    const connections = app.getTile(col, row).connections;
    return (
      connections.top ||
      connections.left ||
      connections.bottom ||
      connections.right
    );
  };

  app.tileIsSolved = function (col, row) {
    const cols = Math.round(app.cols);
    const rows = Math.round(app.rows);
    const connections = app.getTile(col, row).connections;
    return (
      col === 0 ? true : connections.top &&
      row === 0 ? true : connections.left &&
      col === cols - 1 ? true : connections.bottom &&
      row === rows - 1 ? true : connections.right
    );
  };

  app.getHightestTileZ = function () {
    let z = 0;

    app.iterateTiles((col, row) => {
      const tile = app.getTile(col, row);
      if (tile.z > z) {
        z = tile.z;
      }
    });

    return z;
  };

  app.getTile = function (col, row) {
    return app.tiles[row][col];
  };

  function connectedTileExists(col, row, connectedTiles) {
    return connectedTiles
    .map((tile) => {
      return tile.join('-');
    })
    .indexOf(col + '-' + row) !== -1;
  }

  app.collectConnectedTiles = function (col, row, connectedTiles = []) {
    const tile = app.getTile(col, row);
    const cols = Math.round(app.cols);
    const rows = Math.round(app.rows);

    // top
    if (
      row > 0 && tile.connections.top &&
      !connectedTileExists(col, row - 1, connectedTiles)
    ) {
      connectedTiles.push([col, row - 1]);
      app.collectConnectedTiles(col, row - 1, connectedTiles);
    }

    // bottom
    if (
      row < rows - 1 && tile.connections.bottom &&
      !connectedTileExists(col, row + 1, connectedTiles)
    ) {
      connectedTiles.push([col, row + 1]);
      app.collectConnectedTiles(col, row + 1, connectedTiles);
    }

    // left
    if (
      col > 0 && tile.connections.left &&
      !connectedTileExists(col - 1, row, connectedTiles)
    ) {
      connectedTiles.push([col - 1, row]);
      app.collectConnectedTiles(col - 1, row, connectedTiles);
    }

    // right
    if (
      col < cols - 1 && tile.connections.right &&
      !connectedTileExists(col + 1, row, connectedTiles)
    ) {
      connectedTiles.push([col + 1, row]);
      app.collectConnectedTiles(col + 1, row, connectedTiles);
    }

    return connectedTiles;
  };

  app.alignConnectedTiles = function (col, row) {
    const tile = app.getTile(col, row);
    const tileSizeFractionX = 1 / app.cols;
    const tileSizeFractionY = 1 / app.rows;

    app.collectConnectedTiles(col, row)
    .forEach((_coords) => {
      const _col = _coords[0];
      const _row = _coords[1];
      const _tile = app.getTile(_col, _row);
      _tile.x = tile.x + ((_col - col) * tileSizeFractionX);
      _tile.y = tile.y + ((_row - row) * tileSizeFractionY);
    });
  }

  app.solve = function () {
    const cols = Math.round(app.cols);
    const rows = Math.round(app.rows);

    app.iterateTiles((col, row) => {
      const tile = app.getTile(col, row);
      tile.x = col / app.cols;
      tile.y = row / app.rows;

      ['top', 'left', 'bottom', 'right'].forEach((dir) => {
        tile.connections[dir] = true;
      });

      if (col === 0) {
        tile.connections.left = false;
      }
      if (row === 0) {
        tile.connections.top = false;
      }
      if (col === cols) {
        tile.connections.right = false;
      }
      if (row === rows) {
        tile.connections.bottom = false;
      }
    });

    app.storage.setItem('tiles', app.tiles);
    puzzleContainer.classList.add('is-solving');
    app.renderTiles();

    setTimeout(() => {
      puzzleContainer.classList.remove('is-solving');
      puzzleContainer.classList.add('is-solved');
    }, 500);
  };

  app.isSolved = function () {
    let unsolved = 0;

    app.iterateTiles((col, row) => {
      unsolved += app.tileIsSolved(col, row) ? 0 : 1;
    });

    return unsolved === 0;
  };

  app.initTouch = function (touch) {
    app.touches[touch.identifier] = {
      x: touch.clientX,
      y: touch.clientY,
      target: touch.target
    };

    const col = parseInt(touch.target.getAttribute('data-col'), 10);
    const row = parseInt(touch.target.getAttribute('data-row'), 10);
    const zIndex = app.getHightestTileZ() + 1;
    app.getTile(col, row).z = zIndex;

    if (app.tileIsConnected(col, row)) {
      app.collectConnectedTiles(col, row)
      .forEach((_coords) => {
        app.getTile(_coords[0], _coords[1]).z = zIndex;
      });
    }

    touch.target.classList.add('is-dragging');
  };

  app.moveTouch = function (touch) {
    const initialTouch = app.touches[touch.identifier];
    const diffX = touch.clientX - initialTouch.x;
    const diffY = touch.clientY - initialTouch.y;
    const col = parseInt(initialTouch.target.getAttribute('data-col'), 10);
    const row = parseInt(initialTouch.target.getAttribute('data-row'), 10);
    const transform = `translate(${diffX}px, ${diffY}px)`;
    initialTouch.target.style.transform = transform;

    if (app.tileIsConnected(col, row)) {
      app.collectConnectedTiles(col, row)
      .forEach((tile) => {
        const col = tile[0];
        const row = tile[1];
        const el = document.getElementById(`puzzle-tile-${col}-${row}`);
        el.classList.add('is-dragging');
        el.style.transform = transform;
      });
    }
  };

  app.endTouch = function (touch) {
    const tileSize = app.getTileSize();
    const puzzleBounds = puzzleContainer.getBoundingClientRect();
    const puzzleWidth = puzzleContainer.offsetWidth;
    const puzzleHeight = puzzleContainer.offsetHeight;
    const puzzleTop = puzzleBounds.top;
    const puzzleLeft = puzzleBounds.left;
    const tileSizeFractionX = 1 / app.cols;
    const tileSizeFractionY = 1 / app.rows;

    const initialTouch = app.touches[touch.identifier];

    // delete touch
    delete app.touches[touch.identifier];

    // bake new position into tile
    const col = parseInt(initialTouch.target.getAttribute('data-col'), 10);
    const row = parseInt(initialTouch.target.getAttribute('data-row'), 10);
    const tile = app.getTile(col, row);
    const bounds = initialTouch.target.getBoundingClientRect();

    // prevent tiles from disappearing off screen
    const left = _.clamp(bounds.left, 0, window.innerWidth - tileSize);
    const top = _.clamp(bounds.top, 0, window.innerHeight - tileSize);
    tile.x = (left - puzzleLeft) / puzzleWidth;
    tile.y = (top - puzzleTop) / puzzleHeight;

    initialTouch.target.style.transform = null;
    initialTouch.target.classList.remove('is-dragging');

    app.renderTile(col, row);
    let connectedTiles = [];

    if (app.tileIsConnected(col, row)) {
      connectedTiles = app.collectConnectedTiles(col, row);
      connectedTiles
      .forEach((_coords) => {
        const _col = _coords[0];
        const _row = _coords[1];
        const _tile = app.getTile(_col, _row);
        const el = document.getElementById(`puzzle-tile-${_col}-${_row}`);
        el.classList.remove('is-dragging');
        el.style.transform = null;
        _tile.x = tile.x + ((_col - col) * tileSizeFractionX);
        _tile.y = tile.y + ((_row - row) * tileSizeFractionY);
        app.renderTile(_col, _row);
      });
    }

    app.autoConnectTile(col, row);

    // also connect connected tiles
    connectedTiles
    .forEach((_coords) => {
      app.autoConnectTile(_coords[0], _coords[1]);
    });

    if (app.isSolved()) {
      app.solve();
    }
  };

  app.handleTouchStart = function (event) {
    event.preventDefault();

    // create new touches
    Array.from(event.changedTouches)
    .forEach(app.initTouch);

    app.renderTiles();
  };

  app.handleMouseDown = function (event) {
    event.preventDefault();

    event.target.removeEventListener('mousedown', app.handleMouseDown);
    document.body.addEventListener('mousemove', app.handleMouseMove);
    document.body.addEventListener('mouseup', app.handleMouseUp);

    const touch = {
      identifier: 0,
      clientX: event.clientX,
      clientY: event.clientY,
      target: event.target
    };
    app.initTouch(touch);

    app.renderTiles();
  };

  app.handleTouchMove = function (event) {
    event.preventDefault();

    Array.from(event.changedTouches)
    .forEach(app.moveTouch);
  };

  app.handleMouseMove = function (event) {
    event.preventDefault();

    const touch = {
      identifier: 0,
      clientX: event.clientX,
      clientY: event.clientY
    };

    app.moveTouch(touch);
  };

  app.handleTouchEnd = function (event) {
    event.preventDefault();

    Array.from(event.changedTouches)
    .forEach(app.endTouch);

    app.storage.setItem('tiles', app.tiles);
  };

  app.handleTouchCancel = app.handleTouchEnd;

  app.handleMouseUp  = function (event) {
    event.preventDefault();

    app.touches[0].target.addEventListener('mousedown', app.handleMouseDown);
    document.body.removeEventListener('mousemove', app.handleMouseMove);
    document.body.removeEventListener('mouseup', app.handleMouseUp);

    const touch = {
      identifier: 0,
      clientX: event.clientX,
      clientY: event.clientY
    };
    app.endTouch(touch);

    app.storage.setItem('tiles', app.tiles);
  };

  return app;
};

export default PuzzleApp;
